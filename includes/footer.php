<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="col-md-3 footer-1">
				<h3>Company</h3>
				<div class="foot-nav">
					<ul>
						<li class="active"><a href="index.php">Catalogue</a></li>
						<li><a href="members.php">Membres</a></li>
						<li><a href=" search.php">Recherche</a></li>
                        <li><a href=" 404.php">404</a></li>
						<li><a href=" contact.php">Contact</a></li>
							<div class="clearfix"> </div>
					</ul>
				</div>
			</div>
			<div class="col-md-3 footer-1">
				<h3>Social Network</h3>
				<p>Find us on these social network sites</p>
				<div class="social-ic">
					<ul>
						<li><a href="#"><i class="facebok"> </i></a></li>
						<li><a href="#"><i class="twiter"> </i></a></li>
						<li><a href="#"><i class="in"> </i></a></li>
						<li><a href="#"><i class="pp"> </i></a></li>
						<li><a href="#"><i class="goog"> </i></a></li>
							<div class="clearfix"></div>	
					</ul>
				</div>
			</div>
			<div class="col-md-3 footer-1">
				<h3>Location</h3>
				<p>123, street name</p>
				<p>	landmark,</p>
				<p>	California 123</p>
				<p>	Tel: 123-456-7890</p>
				<p>	Fax. +123-456-7890</p>
			</div>
			<div class="col-md-3 footer-1">
				<h3>Text Widget</h3>
				<p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem rutrum venenatis sed laoreet dui. </p>
			</div>
				<div class="clearfix"> </div>
			<div class="foot-bottom">
				<p>Copyrights � 2015 EleganceMedia. All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a></p>
			</div>
		</div>
	</div>
<!-- footer -->

</body>



</html>