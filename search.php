<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once('model/mediaClient.class.php');
?>

<?php

$keyword = '';
$results = null;
if (isset($_GET['search'])) {
    $keyword = $_GET['search'];
    $media_client = mediaClient::Instance();
    $results = $media_client->searchBook($keyword);
}
?>

<?php

$pageTitle = "Ma Médiathèque";

include_once('includes/header.php');
?>

<div id="wrap">
    <form action="" autocomplete="on">
        <input id="search-bar" name="search" type="text" placeholder="Search..."><input id="search_submit"
                                                                                        value="Rechercher"
                                                                                        type="submit">
    </form>
</div>

<!--
<div style="padding-top:12em;float:right;margin-bottom:150px;">
<div class="main">
		<div class="error-404 text-center">
				<h1>404</h1>
				<p>oops! something goes wrong</p>
				<a class="b-home" href="index.php">Back to Home</a>
			</div>
	</div>
</div>
-->

<div class="banner" style="padding-top:12em;">
    <div class="container">
        <?php if ($keyword != '') { ?>
            <h3 style="color:white">Résultats pour : <?php echo $keyword; ?> (<?php echo count($results); ?>
                résultats)</h3>
            <div class="grid">
                <ul id="mybooks">
                    <?php foreach ($results as $book) { ?>
                        <li>
                            <div id="gr-item-" class="grid-item">
                                <div class="banner-1">
                                    <img src="<?php echo $book->getImageUrl(); ?>" class="img-responsive" alt="">
                                    <div class="banner-bottom">
                                        <h4 id="book-title-"><?php echo $book->getName(); ?></h4>
                                        <p id="book-category-"><?php echo $book->getCategory(); ?></p>
                                        <p>Disponible : <?php echo $book->getNb_entities_available(); ?></p>
                                        <!--
                                        <button type="button" class="btn btn-success book-action" >Add</button>
                                        <button type="button" class="btn btn-info book-action edit">Edit</button>
                                        <button type="button" class="btn btn-danger book-action remove">Remove</button>
                                        -->
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>

            </div>
        <?php } ?>
        <div class="clearfix"></div>
    </div>
</div>

<!-- banner -->
<!-- Here -->
<div class="Here">
    <div class="workes" style="margin-left:-100">
        <h2><a href="portfolio.html">See All Our Works</a></h2>
    </div>
    <div class="container">
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="bulb"></i>
            </div>
            <div class="here-right">
                <h5>Your 1st title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere consectetur,
                dolor ipsum tempus justo, quis ornare magna urna id enim. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="lock"></i>
            </div>
            <div class="here-right">
                <h5>Your 2nd title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nulla accumsan libero id quam facilisis nec luctus libero sollicitudin. Sed ligula libero, feugiat sed
                gravida vel, commodo sit amet nibh. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="pen"></i>
            </div>
            <div class="here-right">
                <h5>Your 3rd title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nullam tempus turpis at lorem posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit
                fermentum augue. Etiam eget magna vel ante mattis ultricies vitae ut nunc. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="adm"></i>
            </div>
            <div class="here-right">
                <h5>Your 4th title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nam ac molestie ante. Pellentesque turpis lacus, vulputate vitae feugiat quis, dictum eget diam. Proin
                sapien libero, tempus et tempus ac, pulvinar quis nulla. </p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Here -->
<!-- company -->
<div class="company">
    <div class="container">
        <div class="col-md-4 company-1">
            <h3>Our Company</h3>
            <div class="our">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere
                    consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim. Nulla accumsan libero id quam
                    facilisis nec luctus libero sollicitudin. </p>
            </div>
            <div class="our">
                <p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem
                    posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit fermentum augue.</p>
            </div>
            <div class="our">
                <p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem
                    rutrum venenatis sed laoreet dui. </p>
            </div>
        </div>
        <div class="col-md-4 company-1">
            <h3>Our Testimonials</h3>
            <div class="our">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere
                    consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim.</p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
            <div class="our">
                <p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem
                    posuere sodales. Donec nibh urna, auctor at </p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
            <div class="our">
                <p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem
                    rutrum venenatis sed laoreet dui. </p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
        </div>
        <div class="col-md-4 company-1">
            <h3>From the blog</h3>
            <div class="sleek">
                <h5><a href="single.html">Sleek minimal website PSD template</a></h5>
                <h6>August 30, 2010, 9:32 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">PSD ecommerce website template</a></h5>
                <h6>October 18, 2010, 5:51 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Sleeko- Download single page website design</a></h5>
                <h6>July 22, 2010, 7:19 pm</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Download web buttons in PSD & PNG (pack of 60)</a></h5>
                <h6>September 13, 2010, 2:15 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Flip clock countdown (PSD)</a></h5>
                <h6>August 12, 2011, 10:56 am</h6>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- company -->
<!-- later -->
<div class="later">
    <div class="container">
        <div class="col-md-6 later-right">
            <li><i class="ttt"></i></li>
            <li><h6>Proin enim velit, fermentum at malesuada in, ipsum... <span><a href="#">@graphicsfuel</a><span></h6>
            </li>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- later -->
<?php
include_once('includes/footer.php');
?>