<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php

$pageTitle = "Mes Membres";

include_once('includes/header.php');
require_once('model/mediaClient.class.php');

$members = mediaClient::Instance()->getMembers();

?>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<!--
<div id="message">
    <div style="padding: 5px;">
        <div class="alert alert-success alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Indicates a successful or positive action.
</div>
    </div>
</div>
-->

<section>
    <!--for demo wrap-->
    <h1>Nos Membres</h1>
	
<div class="media-table">
<button style="width:100%;margin-bottom:10px;text-align:center;background-color:yellow;color:grey" type="button" class="btn btn-primary add_member"><i class="fa fa-plus fa-4x" aria-hidden="true"></i> <i class="fa fa-users fa-4x" aria-hidden="true"></i></button>
    <div class="tbl-header">
	
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <th>ID</th>
                <th>Avatar</th>
                <th>Pseudo</th>
                <th>Nom</th>
                <th>Emprunts</th>
                <th>Membre depuis</th>
                <th>Pénalité(s)</th>
                <th>Actions</th>
            </tr>
            </thead>
        </table>
    </div>
    <div class="tbl-content">
        <table cellpadding="0" cellspacing="0" border="0">
            <tbody>
            <?php foreach ($members as $member) { ?>
                <tr id="row_member-<?php echo $member->getId(); ?>">
                    <td>00<?php echo $member->getId(); ?></td>
                    <td><img src="<?php echo $member->getAvatar(); ?>"
                             id="member_avatar-<?php echo $member->getId(); ?>" style="border-radius:50%;width:65px"/>
                    </td>
                    <td>
                        <a href="member-details.php?id_member=<?php echo $member->getId(); ?>"><?php echo $member->getPseudo(); ?></a>
                    </td>
                    <td id="member_name-<?php echo $member->getId(); ?>"><?php echo $member->getName(); ?></td>
                    <td><?php echo $member->getNb_loans(); ?></td>
                    <td><?php echo $member->getSignup_date(); ?></td>
                    <td>
                        <?php echo $member->getPenalityIcon('4x'); ?> (<?php echo $member->getNb_penality(); ?>)
                    </td>
                    <td>
                        <button type="button" class="btn btn-warning edit_member"
                                id="edit_member-<?php echo $member->getId(); ?>" data-toggle="tooltip"
                                title="Mettre à jour profile"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                        <button type="button" class="btn btn-default lock_member"
                                id="lock_member-<?php echo $member->getId(); ?>" data-toggle="tooltip"
                                title="Bloquer le compte"><i class="fa fa-lock" aria-hidden="true"></i></button>
                        <button type="button" class="btn btn-danger delete_member"
                                id="delete_member-<?php echo $member->getId(); ?>" data-toggle="tooltip"
                                title="Supprimer le compte"><i class="fa fa-times" aria-hidden="true"></i></button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</section>

<!-- banner -->
<!-- Here -->
<div class="Here">
    <div class="workes">
        <h2><a href="portfolio.html">See All Our Works</a></h2>
    </div>
    <div class="container">
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="bulb"></i>
            </div>
            <div class="here-right">
                <h5>Your 1st title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere consectetur,
                dolor ipsum tempus justo, quis ornare magna urna id enim. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="lock"></i>
            </div>
            <div class="here-right">
                <h5>Your 2nd title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nulla accumsan libero id quam facilisis nec luctus libero sollicitudin. Sed ligula libero, feugiat sed
                gravida vel, commodo sit amet nibh. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="pen"></i>
            </div>
            <div class="here-right">
                <h5>Your 3rd title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nullam tempus turpis at lorem posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit
                fermentum augue. Etiam eget magna vel ante mattis ultricies vitae ut nunc. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="adm"></i>
            </div>
            <div class="here-right">
                <h5>Your 4th title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nam ac molestie ante. Pellentesque turpis lacus, vulputate vitae feugiat quis, dictum eget diam. Proin
                sapien libero, tempus et tempus ac, pulvinar quis nulla. </p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Here -->
<!-- company -->
<div class="company">
    <div class="container">
        <div class="col-md-4 company-1">
            <h3>Our Company</h3>
            <div class="our">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere
                    consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim. Nulla accumsan libero id quam
                    facilisis nec luctus libero sollicitudin. </p>
            </div>
            <div class="our">
                <p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem
                    posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit fermentum augue.</p>
            </div>
            <div class="our">
                <p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem
                    rutrum venenatis sed laoreet dui. </p>
            </div>
        </div>
        <div class="col-md-4 company-1">
            <h3>Our Testimonials</h3>
            <div class="our">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere
                    consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim.</p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
            <div class="our">
                <p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem
                    posuere sodales. Donec nibh urna, auctor at </p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
            <div class="our">
                <p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem
                    rutrum venenatis sed laoreet dui. </p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
        </div>
        <div class="col-md-4 company-1">
            <h3>From the blog</h3>
            <div class="sleek">
                <h5><a href="single.html">Sleek minimal website PSD template</a></h5>
                <h6>August 30, 2010, 9:32 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">PSD ecommerce website template</a></h5>
                <h6>October 18, 2010, 5:51 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Sleeko- Download single page website design</a></h5>
                <h6>July 22, 2010, 7:19 pm</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Download web buttons in PSD & PNG (pack of 60)</a></h5>
                <h6>September 13, 2010, 2:15 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Flip clock countdown (PSD)</a></h5>
                <h6>August 12, 2011, 10:56 am</h6>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- company -->
<!-- later -->

<!-- later -->
<?php
include_once('includes/footer.php');
?>