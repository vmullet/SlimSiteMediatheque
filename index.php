<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once('model/mediaClient.class.php');

$media_client = mediaClient::Instance();
$books = $media_client->getBooks();

?>


<?php

$pageTitle = "Ma Médiathèque";

include_once('includes/header.php');

?>

<div id="isotope-menu">
    <ul>
        <li class="iso-option"><a href="#" data-filter="*">Tous</a></li>
        <li class="iso-option"><a href="#" data-filter=".fantastique">Fantastique</a></li>
        <li class="iso-option"><a href="#" data-filter=".fantasy">Fantasy</a></li>
    </ul>
</div>
<button style="margin-top:15px;float:right;font-size:15px;" type="button" class="btn btn-primary book-action add-book"><i class="fa fa-plus fa-4x" aria-hidden="true"></i> <i class="fa fa-book fa-4x" aria-hidden="true"></i></button>
<div class="banner">
    <div class="container">
        <div class="grid">
            <ul id="mybooks">
                <?php foreach ($books as $book) { ?>
                    <li>
                        <div id="gr-item-<?php echo $book->getId(); ?>"
                             class="grid-item <?php echo $book->getCategory(); ?> gr-item-<?php echo $book->getId(); ?>">
                            <div class="banner-1">
                                <a href="book-details.php?id_book=<?php echo $book->getId(); ?>"><img
                                            src="<?php echo $book->getImageUrl(); ?>" class="img-responsive" id="book_image_url-<?php echo $book->getId(); ?>" alt=""></a>
                                <div class="banner-bottom">
                                    <a href="book-details.php?id_book=<?php echo $book->getId(); ?>"><h4
                                                id="book_title-<?php echo $book->getId(); ?>"><?php echo $book->getName(); ?></h4>
                                    </a>
                                    <p id="book_category-<?php echo $book->getId(); ?>"><?php echo $book->getCategory(); ?></p>
                                    <h5 style="color:white"
                                        id="book_author-<?php echo $book->getId(); ?>"><?php echo $book->getAuthor(); ?></h5>
                                    <button type="button" class="btn btn-info book-action edit-book"
                                            id="<?php echo 'edit_book-' . $book->getId(); ?>">Edit
                                    </button>
                                    <button type="button" class="btn btn-danger book-action delete-book"
                                            id="<?php echo 'remove_book-' . $book->getId(); ?>">Remove
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>

                <?php } ?>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- banner -->
<!-- Here -->
<div class="Here">
    <div class="workes">
        <h2><a href="portfolio.html">See All Our Works</a></h2>
    </div>
    <div class="container">
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="bulb"></i>
            </div>
            <div class="here-right">
                <h5>Your 1st title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere consectetur,
                dolor ipsum tempus justo, quis ornare magna urna id enim. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="lock"></i>
            </div>
            <div class="here-right">
                <h5>Your 2nd title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nulla accumsan libero id quam facilisis nec luctus libero sollicitudin. Sed ligula libero, feugiat sed
                gravida vel, commodo sit amet nibh. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="pen"></i>
            </div>
            <div class="here-right">
                <h5>Your 3rd title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nullam tempus turpis at lorem posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit
                fermentum augue. Etiam eget magna vel ante mattis ultricies vitae ut nunc. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="adm"></i>
            </div>
            <div class="here-right">
                <h5>Your 4th title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nam ac molestie ante. Pellentesque turpis lacus, vulputate vitae feugiat quis, dictum eget diam. Proin
                sapien libero, tempus et tempus ac, pulvinar quis nulla. </p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Here -->
<!-- company -->
<div class="company">
    <div class="container">
        <div class="col-md-4 company-1">
            <h3>Our Company</h3>
            <div class="our">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere
                    consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim. Nulla accumsan libero id quam
                    facilisis nec luctus libero sollicitudin. </p>
            </div>
            <div class="our">
                <p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem
                    posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit fermentum augue.</p>
            </div>
            <div class="our">
                <p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem
                    rutrum venenatis sed laoreet dui. </p>
            </div>
        </div>
        <div class="col-md-4 company-1">
            <h3>Our Testimonials</h3>
            <div class="our">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere
                    consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim.</p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
            <div class="our">
                <p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem
                    posuere sodales. Donec nibh urna, auctor at </p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
            <div class="our">
                <p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem
                    rutrum venenatis sed laoreet dui. </p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
        </div>
        <div class="col-md-4 company-1">
            <h3>From the blog</h3>
            <div class="sleek">
                <h5><a href="single.html">Sleek minimal website PSD template</a></h5>
                <h6>August 30, 2010, 9:32 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">PSD ecommerce website template</a></h5>
                <h6>October 18, 2010, 5:51 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Sleeko- Download single page website design</a></h5>
                <h6>July 22, 2010, 7:19 pm</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Download web buttons in PSD & PNG (pack of 60)</a></h5>
                <h6>September 13, 2010, 2:15 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Flip clock countdown (PSD)</a></h5>
                <h6>August 12, 2011, 10:56 am</h6>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- company -->
<!-- later -->
<!-- later -->
<?php
include_once('includes/footer.php');
?>