<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
$pageTitle = 'Erreur 404';
include_once('includes/header.php');
?>
<!-- header -->
<!-- 404 -->
	<div class="main">
		<div class="error-404 text-center">
				<h1>404</h1>
				<p>oops! Une erreur est survenue</p>
				<a class="b-home" href="index.php">Retour vers l'accueil</a>
			</div>
	</div>
	<!-- 404 -->

<!-- later -->

<!-- later -->
<!-- footer -->
<?php
include_once('includes/footer.php');
?>
<!-- footer -->
</body>
</html>