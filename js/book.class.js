let bookInstance = null;

class bookJs {

	constructor() {
        if (!bookInstance) {
            bookInstance = this;
        }
        return bookInstance;
    }
	
	getAddForm(id_book) {
		
		let book_name = $('#book_title-'+id_book).text();
        let book_category = $('#book_category-'+id_book).text();
        let book_author = $('#book_author-'+id_book).text();
        let myform='';
        myform += '<div class="form-group" style="display:inline-block;float:left;margin-right:10px">';
        myform += '<label>Titre</label>';
        myform += "<input class='form-control' id='new_book_name-00' type='text' value=''>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:10px;">';
        myform += '<label>Catégorie</label>';
        myform += "<input class='form-control' id='new_book_category-00' type='text'  value=''>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:10px;">';
        myform += '<label>Auteur</label>';
        myform += "<input class='form-control' id='new_book_author-00' type='text'  value=''>";
        myform += '</div>';
		myform += '<div class="form-group" style="display:inline-block">';
        myform += '<label>Image Url</label>';
        myform += "<input class='form-control' id='new_book_image_url-00' type='text'  value=''>";
        myform += '</div>';
        return myform;
		
	}
	
	getEditForm(id_book,book_details) {
        let book_name = '';
        let book_category = '';
        let book_author = '';
        if (book_details) {
            book_name = $('#book_title-'+id_book).text().split(': ')[1];
            book_category = $('#book_category-'+id_book).text().split(': ')[1];
            book_author = $('#book_author-'+id_book).text().split(': ')[1];
        }
        else {
            book_name = $('#book_title-'+id_book).text();
            book_category = $('#book_category-'+id_book).text();
            book_author = $('#book_author-'+id_book).text();
        }

		let book_image_url = $('#book_image_url-'+id_book).attr('src');
        let myform='';
        myform += '<div class="form-group" style="display:inline-block;float:left;margin-right:10px">';
        myform += '<label>Titre</label>';
        myform += "<input class='form-control' id='new_book_name-"+id_book+"' type='text' value='"+this.escapeHtml(book_name)+"'>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:10px;">';
        myform += '<label>Catégorie</label>';
        myform += "<input class='form-control' id='new_book_category-"+id_book+"' type='text'  value='"+this.escapeHtml(book_category)+"'>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:10px;">';
        myform += '<label>Auteur</label>';
        myform += "<input class='form-control' id='new_book_author-"+id_book+"' type='text'  value='"+this.escapeHtml(book_author)+"'>";
        myform += '</div>';
		myform += '<div class="form-group" style="display:inline-block">';
        myform += '<label>Image Url</label>';
        myform += "<input class='form-control' id='new_book_image_url-"+id_book+"' type='text'  value='"+this.escapeHtml(book_image_url)+"'>";
        myform += '</div>';
        return myform;
		
	}
	
	addBook(book) {
		
		let dialog = bootbox.dialog({
            title: 'Ajout en cours',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Ajout...</p><br>'

        });

        dialog.find('.bootbox-close-button').hide();

        $.post('action/book-action.php',
            {
                action:'add',
                book_name:book['book_name'],
				book_category:book['book_category'],
                book_author:book['book_author'],
				book_image_url:book['book_image_url']
            },function(data) {
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Ajouté avec succès');   
                }
                else {
                    dialog.find('.bootbox-body').html('Erreur lors de l\'ajout');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });
		
		
	}
	
	
	editBook(book,book_details) {
		
		let dialog = bootbox.dialog({
            title: 'Modification en cours',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Modification...</p><br>'

        });

        dialog.find('.bootbox-close-button').hide();

        $.post('action/book-action.php',
            {
                action:'update',
                id_book:book['book_id'],
                book_name:book['book_name'],
                book_category:book['book_category'],
                book_author:book['book_author'],
				book_image_url:book['book_image_url']
            },function(data) {
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Modification effectuée avec succès');
                    if (book_details) {
                        $('#book_title-' + book['book_id']).text('Titre : ' + book['book_name']);
                        $('#book_category-' + book['book_id']).text('Catégorie : ' + book['book_category']);
                        $('#book_author-' + book['book_id']).text('Auteur : ' + book['book_author']);
                        $('#book_image_url-' + book['book_id']).attr('src',book['book_image_url']);
                    } else {
                        $('#book_title-' + book['book_id']).text(book['book_name']);
                        $('#book_category-' + book['book_id']).text(book['book_category']);
                        $('#book_author-' + book['book_id']).text(book['book_author']);
                        $('#book_image_url-' + book['book_id']).attr('src',book['book_image_url']);
                    }

                }
                else {
                    dialog.find('.bootbox-body').html('Erreur lors de la modification '+book['book_id']+' '+book['book_name']+' '+book['book_category']+' '+book['book_author']+' '+book['book_image_url']);
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });
		
	}
	
	deleteBook(id_book) {
		
		let dialog = bootbox.dialog({
            title: 'Suppression en cours',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Suppression...</p><br>'

        });

        $.post('action/book-action.php',
            {
                action:'delete',
                id_book:id_book
            },function(data) {
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Supprimé avec succès');
                    let removeItem = $('#gr-item-'+id_book);
                    $grid.isotope('remove',removeItem);
                }
                else {
                    dialog.find('.bootbox-body').html('Echec lors de la suppression');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });
		
	}
	
	escapeHtml(text) {
		
	var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
	};

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
  
  }

	
}