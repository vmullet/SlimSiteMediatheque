$(function() {

    let $grid = $('.grid').isotope({
        // options
        itemSelector: '.grid-item',
        layoutMode: 'fitRows'
    });

    $('.iso-option a').on('click',function() {
        let the_filter = $(this).attr('data-filter');
        $grid.isotope({ filter: the_filter });
    });
	
	$('.add-book').on('click',function() {

        let dialog = bootbox.confirm({
            title: 'Ajouter un livre',
            message: new bookJs().getAddForm(),
            buttons: {
                confirm: {
                    label: 'Créer',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
                callback: function (result) {
                    if (result) {
                        let book_name = $('#new_book_name-00').val();
                        let book_category = $('#new_book_category-00').val();
                        let book_author = $('#new_book_author-00').val();
						let book_image_url = $('#new_book_image_url-00').val();
                        let args = {'book_name' : book_name , 'book_category' : book_category , 'book_author' : book_author , 'book_image_url' : book_image_url };
                        new bookJs().addBook(args);
                    }
                }

        });

    });

    $('.edit-book').on('click',function() {

        let id_book = $(this).attr('id').split('-')[1];
        let action = $(this).attr('id').split('-')[0];
        let form_body = '';
        if ($(this).attr('id').split('-')[0]=='edit_book_details') {
            form_body = new bookJs().getEditForm(id_book,true)
        }
        else {
            form_body = new bookJs().getEditForm(id_book,false)
        }
        let dialog = bootbox.confirm({
            title: 'Modifier un livre',
            message: form_body,
            buttons: {
                confirm: {
                    label: 'Modifier',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
                callback: function (result) {
                    if (result) {
                        let book_name = $('#new_book_name-'+id_book).val();
                        let book_category = $('#new_book_category-'+id_book).val();
                        let book_author = $('#new_book_author-'+id_book).val();
						let book_image_url = $('#new_book_image_url-'+id_book).val();
                        let args = {'book_id': id_book , 'book_name' : book_name , 'book_category' : book_category , 'book_author' : book_author , 'book_image_url' : book_image_url };
                        if (action=='edit_book_details') {
                            new bookJs().editBook(args,true);
                        } else {
                            new bookJs().editBook(args,false);
                        }
                    }
                }

        });

    });

    $('.delete-book').on('click',function() {

        let id_book = $(this).attr('id').split('-')[1];
        let dialog = bootbox.confirm({
            title: 'Supprimer un livre',
            message: 'Are you sure you want to remove ' + $('#book_title-' + id_book).text() + '?',
            buttons: {
                confirm: {
                    label: 'Supprimer',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
                callback: function (result) {
                if (result) {
                    new bookJs().deleteBook(id_book);
                }


                }

        });

    });
	
	$('.add_member').on('click',function(){
        
        let dialog = bootbox.confirm({
            title: 'Ajouter un membre',
            message: new memberJs().getAddForm(),
            buttons: {
                confirm: {
                    label: 'Ajouter',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    let args = 
					{
						'pseudo' : $('#new_member_pseudo-00').val(),
						'name' : $('#new_member_name-00').val(),
						'avatar' : $('#new_member_avatar-00').val()
					};
					
                    new memberJs().addMember(args);
                }
            }

        });
    });

    $('.edit_member').on('click',function(){
        let id_member = $(this).attr('id').split('-')[1];
        let dialog = bootbox.confirm({
            title: 'Editer Profil du membre',
            message: new memberJs().getEditForm(id_member),
            buttons: {
                confirm: {
                    label: 'Modifier',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    let member_name = $('#new_member_name-'+id_member).val();
                    let member_avatar = $('#new_member_avatar-'+id_member).val();
                    let args = {'id':id_member,'name':member_name,'avatar':member_avatar};
                    new memberJs().editMember(args);
                }
            }

        });
    });

    $('.delete_member').on('click',function() {
        let id_member = $(this).attr('id').split('-')[1];
        let member_name = $('#member_name-'+id_member).text();
        let dialog = bootbox.confirm({
            title: 'Supprimer membre',
            message: 'Etes-vous sûr de vouloir supprimer '+ member_name + '?',
            buttons: {
                confirm: {
                    label: 'Supprimer',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    new memberJs().deleteMember(id_member);
                }
            }

        });

    });
	
	$('.add_entity').on('click',function(){
        let id_book = $(this).attr('id').split('-')[1];
        let dialog = bootbox.confirm({
            title: 'Ajouter un exemplaire',
            message: new entityJs().getAddForm(id_book),
            buttons: {
                confirm: {
                    label: 'Ajouter',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
				
				let available = $('#new_entity_available-00').is(":checked") ? 1 : 0;
                if (result) {
                    let args = {
						'id_book':id_book,
						'id_entity':$('#new_entity_id-00').val(),
						'nb_times_borrowed':$('#new_entity_borrowed-00').val(),
						'item_status_rate':$('#new_entity_rate-00').val(),
						'available':available
					}
					new entityJs().addEntity(args);
                }
            }

        });
    });
	
	
	$('.edit_entity').on('click',function(){
        let id_entity = $(this).attr('id').split('-')[1];
        let dialog = bootbox.confirm({
            title: 'Editer Exemplaire',
            message: new entityJs().getEditForm(id_entity),
            buttons: {
                confirm: {
                    label: 'Valider',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
					let available = $('#new_entity_available-'+id_entity).is(":checked") ? 1 : 0;
                    let args = {
						'id_entity':$('#new_entity_id-'+id_entity).val(),
						'nb_times_borrowed':$('#new_entity_borrowed-'+id_entity).val(),
						'item_status_rate':$('#new_entity_rate-'+id_entity).val(),
						'available':available
					}
					new entityJs().editEntity(args);
                }
            }

        });
    });
	
	
	$('.delete_entity').on('click',function(){
        let id_entity = $(this).attr('id').split('-')[1];
        let dialog = bootbox.confirm({
            title: 'Supprimer exemplaire',
            message: 'Etes-vous sûr de vouloir supprimer l\'exemplaire ' + id_entity + ' ?',
            buttons: {
                confirm: {
                    label: 'Valider',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    new entityJs().deleteEntity(id_entity);
                }
            }

        });
    });
	
	$('.add_loan').on('click',function(){
		
		let members = '';
		let id_entity = $(this).attr('id').split('-')[1];
        $.ajax({
                url : 'member-action.php',
                type : 'POST',
                dataType : 'json',
                data : {'action':'get'},
                success : function (data)
                {
					let dialog = bootbox.confirm({
					title: 'Ajouter un emprunt',
					message: new loanJs().getAddForm(id_entity,data),
					buttons: {
					confirm: {
                    label: 'Valider',
                    className: 'btn-success'
					},
					cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
					}
				},
				callback: function (result) {
                if (result) {
					let finished = $('#new_finished-00').is(":checked") ? 1 : 0;
					let args = {
						'id_member': $('#new_member-00 option:selected').val(),
						'id_entity': $('#new_entity_id-00').val(),
						'start_date': $('#new_start_date-00').val(),
						'end_date': $('#new_end_date-00').val(),
						'finished': finished
					}
					
                    new loanJs().addLoan(args);
                }
            }

        });

                },
				error : function (data) {
					
				}
            });
        
		
    });
	
	$('.edit_loan').on('click',function(){
        let id_loan = $(this).attr('id').split('-')[1];
        let dialog = bootbox.confirm({
            title: 'Editer un emprunt',
            message: new loanJs().getEditForm(id_loan),
            buttons: {
                confirm: {
                    label: 'Valider',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
					let finished = $('#new_finished-'+id_loan).is(":checked") ? 1 : 0;
					let args = {
						'id_loan': id_loan,
						'start_date': $('#new_start_date-'+id_loan).val(),
						'end_date': $('#new_end_date-'+id_loan).val(),
						'finished': finished
					}
                    new loanJs().editLoan(args);
                }
            }

        });
	
    });
	
	$('.delete_loan').on('click',function(){
        let id_loan = $(this).attr('id').split('-')[1];
        let dialog = bootbox.confirm({
            title: 'Supprimer un emprunt',
            message: 'Etes-vous sûr de vouloir supprimer l\'emprunt ' + id_loan + ' ?',
            buttons: {
                confirm: {
                    label: 'Valider',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    new loanJs().deleteLoan(id_loan);
                }
            }

        });
    });

});
