let loanInstance = null;

class loanJs {

	constructor() {
        if (!loanInstance) {
            loanInstance = this;
        }
        return loanInstance;
    }

	getEditForm(id_loan) {
        let myform = "";
		
		let member = $("#hidden_id_member").val();
		let id_entity = $("#loan_entity_id-"+id_loan).text();
        let start_date = $("#loan_start_date-"+id_loan).text();
		let end_date = $("#loan_end_date-"+id_loan).text();
		let finished = $("#loan_is_finished-"+id_loan).text();
		let checked = '';
		if (finished=='Oui') {
			checked = 'checked';
		}
		
		myform += '<fieldset>';
        myform += '<div class="form-group" style="display:inline-block;float:left;margin-right:10px">';
        myform += '<label>ID Membre</label>';
        myform += "<input class='form-control' id='new_member_name-"+id_loan+"' type='text' value='"+member+"' disabled>";
        myform += '</div>';
		myform += '<div class="form-group" style="display:inline-block;float:left;margin-right:10px">';
        myform += '<label>ID Entité (N° Série)</label>';
        myform += "<input class='form-control' id='new_entity_id-"+id_loan+"' type='text' value='"+id_entity+"' disabled>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:10px">';
        myform += '<label for="dtp_input2" class="control-label">Date de début</label>';
		myform += '<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">';
        myform += "<input class='form-control' type='text' id='new_start_date-"+id_loan+"' value='"+start_date+"' readonly>";
		myform += '<span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>';
		myform += '</div>';
		myform += '<input type="hidden" id="dtp_input2" value="" />';
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:5px;">';
        myform += '<label>Date de fin</label>';
		myform += '<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">';
        myform += "<input class='form-control' type='text' id='new_end_date-"+id_loan+"' value='"+end_date+"' readonly>";
		myform += '<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>';
		myform += '</label>';
		myform += '</div>';
        myform += '</div>';
		myform += '<br />';
		myform += '<div class="form-group" style="display:inline-block">';
        myform += '<label>Rendu</label>';
        myform += "<input class='form-control' id='new_finished-"+id_loan+"' type='checkbox' "+checked+" >";
        myform += '</div>';
		myform += '</fieldset>';
		myform += '<script>$(".form_date").datetimepicker({language:  "fr",'
        + 'weekStart: 1,'
        + 'todayBtn:  1,'
		+ 'autoclose: 1,'
		+ 'todayHighlight: 1,'
		+ 'startView: 2,'
		+ 'minView: 2,'
		+ 'forceParse: 0});</script>';

        return myform;
		
    }
	
	getAddForm(id_entity,members) {
		let myform = "";
		members = JSON.parse(members);
		
		myform += '<fieldset>';
        myform += '<div class="form-group" style="display:inline-block;float:left;margin-right:10px">';
        myform += '<label>Membre</label>';
        myform += "<select class='form-control' id='new_member-00'>";
		
		$.each(members, function(i, item){
		myform += "<option value='"+item.id+"'>"+item.name+"</option>";
		});
		
		myform += '</select>';
        myform += '</div>';
		myform += '<div class="form-group" style="display:inline-block;float:left;margin-right:10px">';
        myform += '<label>ID Entité (N° Série)</label>';
        myform += "<input class='form-control' id='new_entity_id-00' type='text' value='"+id_entity+"' disabled>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:10px">';
        myform += '<label for="dtp_input2" class="control-label">Date de début</label>';
		myform += '<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">';
        myform += "<input class='form-control' type='text' id='new_start_date-00' value='' readonly>";
		myform += '<span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>';
		myform += '</div>';
		myform += '<input type="hidden" id="dtp_input2" value="" />';
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:5px;">';
        myform += '<label>Date de fin</label>';
		myform += '<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">';
        myform += "<input class='form-control' type='text' id='new_end_date-00' value='' readonly>";
		myform += '<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>';
		myform += '</label>';
		myform += '</div>';
        myform += '</div>';
		myform += '<br />';
		myform += '<div class="form-group" style="display:inline-block">';
        myform += '<label>Rendu</label>';
        myform += "<input class='form-control' id='new_finished-00' type='checkbox' >";
        myform += '</div>';
		myform += '</fieldset>';
		myform += '<script>$(".form_date").datetimepicker({language:  "fr",'
        + 'weekStart: 1,'
        + 'todayBtn:  1,'
		+ 'autoclose: 1,'
		+ 'todayHighlight: 1,'
		+ 'startView: 2,'
		+ 'minView: 2,'
		+ 'forceParse: 0});</script>';

        return myform;
	}
	
	addLoan(loan) {
		
		let dialog = bootbox.dialog({
            title: 'Ajout en cours ',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Ajout...</p><br>'

        });

        $.post('action/loan-action.php',
            {
                'action':'add',
				'id_member':loan['id_member'],
                'id_entity':loan['id_entity'],
                'start_date':loan['start_date'],
                'end_date':loan['end_date'],
				'finished':loan['finished']
            },function(data) {
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Ajouté avec succès');
                }
                else {
                    dialog.find('.bootbox-body').html('Erreur lors de l\'ajout');
					console.debug(data);
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });

		
	}
	
	editLoan(loan) {
		
		let dialog = bootbox.dialog({
            title: 'Modifcation en cours '+loan['id_loan'],
            message: '<p><i class="fa fa-spin fa-spinner"></i> Modification...</p><br>'

        });

        $.post('action/loan-action.php',
            {
                'action':'update',
                'id_loan':loan['id_loan'],
                'start_date':loan['start_date'],
                'end_date':loan['end_date'],
				'finished':loan['finished']
            },function(data) {
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Modifié avec succès');
                }
                else {
                    dialog.find('.bootbox-body').html('Erreur lors de la modifcation ');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });

		
	}
	
	deleteLoan(id_loan) {
		
		let dialog = bootbox.dialog({
            title: 'Suppression de l\'emprunt '+id_loan,
            message: '<p><i class="fa fa-spin fa-spinner"></i> Suppression en cours...</p><br>'

        });

        $.post('action/loan-action.php',
            {
                'action':'delete',
                'id_loan':id_loan
            },function(data) {
            console.log(data);
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Supprimé avec succès');
					$('#row_loan-' + id_loan).remove();
                }
                else {
                    dialog.find('.bootbox-body').html('Echec lors de la suppression');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });
		
	}
	
	escapeHtml(text) {
		
	var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
	};

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
  
  }
  
  
	
}