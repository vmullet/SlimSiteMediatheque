let memberInstance = null;

class memberJs {

    constructor() {
        if (!memberInstance) {
            memberInstance = this;
        }
        return memberInstance;
    }

    getEditForm(id_member) {
        let myform = "";
        let member_name = $("#member_name-"+id_member).text();
        let member_avatar = $("#member_avatar-"+id_member).attr("src");
		myform += '<div class="form-group" style="display:inline-block;float:left;margin-right:10px">'
			   + "<label>Nom</label>"
			   + "<input class='form-control' id='new_member_name-"+id_member+"' type=text value='"+member_name+"'>"
               + "</div>"
               + '<div class="form-group" style="display:inline-block;margin-right:10px">'
               + "<label>Avatar</label>"
               + "<input class='form-control' id='new_member_avatar-"+id_member+"' type=text value='"+member_avatar+"'>"
               + "</div>";
        return myform;
    }
	
	getAddForm() {
		let myform = '';
		myform += '<div class="form-group">'
			   + "<label>Pseudo</label>"
			   + "<input class='form-control' id='new_member_pseudo-00' type='text' value=''>"
               + "</div>"
			   + '<div class="form-group">'
			   + "<label>Nom</label>"
			   + "<input class='form-control' id='new_member_name-00' type='text' value=''>"
               + "</div>"
               + '<div class="form-group">'
               + "<label>Avatar</label>"
               + "<input class='form-control' id='new_member_avatar-00' type='text' value=''>"
               + "</div>";
		
		return myform;
		
	}
	
	addMember(member) {
		
		let dialog = bootbox.dialog({
            title: 'Ajout en cours',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Ajout...</p><br>'

        });

        $.post('action/member-action.php',
            {
                'action':'add',
                'member_pseudo':member['pseudo'],
                'member_name':member['name'],
                'member_avatar':member['avatar']
            },function(data) {
				
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Ajouté avec succès');
                }
                else {
                    dialog.find('.bootbox-body').html('Erreur lors de l\'ajout');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });
		
	}

    editMember(member) {

        let dialog = bootbox.dialog({
            title: 'Modification en cours',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Modification...</p><br>'

        });

        $.post('action/member-action.php',
            {
                'action':'update',
                'id_member':member['id'],
                'member_name':member['name'],
                'member_avatar':member['avatar']
            },function(data) {
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Mis à jour avec succès');
                    $('#member_name-' + member['id']).text(member['name']);
                    $('#member_avatar-' + member['id']).attr('src',member['avatar']);
                }
                else {
                    dialog.find('.bootbox-body').html('Erreur lors de la mise à jour');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });

    }

    deleteMember(id_member) {
        let dialog = bootbox.dialog({
            title: 'Suppression en cours',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Suppression...</p><br>'

        });

        $.post('action/member-action.php',
            {
                'action':'delete',
                'id_member':id_member
            },function(data) {
            
                if (data=='Success') {
					$('#row_member-' + id_member).remove();
                    dialog.find('.bootbox-body').html('Suppression effectuée avec succès');
                }
                else {
                    dialog.find('.bootbox-body').html('Erreur lors de la suppression');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });
    }
}