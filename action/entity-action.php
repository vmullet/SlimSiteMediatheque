<?php

/**
Php file to handle actions related to the entity table
**/

require_once ('../vendor/autoload.php');
require_once('../model/mediaClient.class.php');

$client = new GuzzleHttp\Client();

$action = '';
$id_book = '';
$id_entity = '';

if (isset($_POST['action'])) {
    $action = $_POST['action']; // Action performed (add,edit,delete)
}

if (isset($_POST['id_book'])) {
    $id_book = $_POST['id_book'];
}

if (isset($_POST['id_entity'])) {
    $id_entity = $_POST['id_entity'];
}

switch ($action) {

    case 'add':
        $nb_times_borrowed = $_POST['nb_times_borrowed'];
        $item_status_rate = $_POST['item_status_rate'];
        $available = $_POST['available'];
        $request = $client->post(mediaClient::URL_SERVICE.'books/'.$id_book.'/entity', array(
                'json' => array(
					'id_entity' => $id_entity,
                    'nb_times_borrowed' => $nb_times_borrowed,
                    'item_status_rate' => $item_status_rate,
                    'available' => $available
                )
            )
        );
        $response = json_decode($request->getBody()->getContents(), true);
        if (isset($response['id'])) { // In slimservice api, every methods returns the object with its id when successful except DELETE
            echo 'Success';
        } else {
            echo 'Fail';
        }
        break;

    case 'update':
        $nb_times_borrowed = $_POST['nb_times_borrowed'];
        $item_status_rate = $_POST['item_status_rate'];
        $available = $_POST['available'];
        $request = $client->put(mediaClient::URL_SERVICE.'entity/'.$id_entity, array(
                'json' => array(
                    'nb_times_borrowed' => $nb_times_borrowed,
                    'item_status_rate' => $item_status_rate,
                    'available' => $available
                )
            )
        );
        $response = json_decode($request->getBody()->getContents(), true);
        if (isset($response['id'])) { // In slimservice api, every methods returns the object with its id when successful except DELETE
            echo 'Success';
        } else {
            echo 'Fail';
        }
        break;

    case 'delete':
        $response = $client->delete(mediaClient::URL_SERVICE.'entity/'.$id_entity);
        $response = $response->getBody()->getContents();
        if ($response == '1') { // In slimservice api, DELETE requests return '1' whens successful
            echo 'Success';
        } else {
            echo 'Fail'.' '.$response;
        }
        break;

   
    default:
        echo "I don't know what to do :p";
        break;

}


?>
