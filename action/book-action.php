<?php

/**
Php file to handle all POST/PUT/DELETE requests related to book table. Theis file will be called by AJAX
**/

require_once ('../vendor/autoload.php');
require_once('../model/mediaClient.class.php');

$client = new GuzzleHttp\Client();

$action = '';
$id_book = '';

if (isset($_POST['action'])) {
	$action = $_POST['action']; // Action performed (add,edit,delete)
}

if (isset($_POST['id_book'])) {
	$id_book = $_POST['id_book'];
}


switch ($action) {

    case 'add':
		$book_name = $_POST['book_name'];
        $book_category = $_POST['book_category'];
		$book_author = $_POST['book_author'];
		$book_image_url = $_POST['book_image_url'];
        $response = $client->post(mediaClient::URL_SERVICE.'books',array(
            'json' => array(
            'name' => $book_name,
            'category' => $book_category,
			'author' => $book_author,
			'image_url' => $book_image_url
        )
            )
        );
		$response = json_decode($response->getBody()->getContents(),true);
		if (isset($response['id'])) {  // In slimservice api, every methods returns the object with its id when successful except DELETE
			echo 'Success';
		} else {
			echo 'Fail';
		}
        break;

    case 'update':
        $book_name = $_POST['book_name'];
        $book_category = $_POST['book_category'];
        $book_author = $_POST['book_author'];
		$book_image_url = $_POST['book_image_url'];
        $response = $client->put(mediaClient::URL_SERVICE.'books/'.$id_book,array(
            'json' => array(
            'name' => $book_name,
            'category' => $book_category,
            'author' => $book_author,
			'image_url' => $book_image_url
        )
            )
        );
		$response = json_decode($response->getBody()->getContents(),true);
		if (isset($response['id'])) { // In slimservice api, every methods returns the object with its id when successful except DELETE
			echo 'Success';
		} else {
			echo 'Fail';
		}
        break;

    case 'delete':
        $response = $client->delete(mediaClient::URL_SERVICE.'books/'.$id_book);
        $response = $response->getBody()->getContents();
		if ($response=='1') { // In slimservice api, DELETE requests return '1' when successful
			echo 'Success';
		} else {
			echo 'Fail';
		}
        break;

    default:
        echo "I don't know what to do :p";
        break;

}




?>
