-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  sam. 01 avr. 2017 à 19:42
-- Version du serveur :  5.5.54-0+deb8u1
-- Version de PHP :  7.0.17-1~dotdeb+8.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mediatheque`
--

-- --------------------------------------------------------

--
-- Structure de la table `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL,
  `image_url` varchar(150) NOT NULL,
  `category` varchar(50) NOT NULL,
  `author` varchar(75) NOT NULL,
  `stock_total` int(11) NOT NULL,
  `stock_dispo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `book`
--

INSERT INTO `book` (`id`, `name`, `image_url`, `category`, `author`, `stock_total`, `stock_dispo`) VALUES
(1, 'Harry Potter et la Coupe de Feu', 'http://www.encyclopedie-hp.org/wp-content/uploads/sites/4/2014/09/couverture_cf.jpg', 'Fantastique', 'J.K Rowling', 4, 3),
(3, 'Harry Potter et l\'Ordre du Phénix', 'http://www.babelio.com/couv/10230_671173.gif', 'Fantastique', 'J.K Rowling', 4, 1),
(4, 'Harry Potter à l\'école des sorciers', 'http://static.fnac-static.com/multimedia/images_produits/ZoomPE/0/7/2/9782070541270/tsp20130828215601/Harry-Potter-a-l-ecole-des-sorciers.jpg', 'Fantastique', 'J.K Rowling', 2, 2),
(5, 'Le seigneur des anneaux : la communauté de l\'anneau', 'http://a.decitre.di-static.com/img/fullsize/j-r-r-tolkien-le-seigneur-des-anneaux-tome-1-la-communaute-de-l-anneau/9782266154116FS.gif', 'Fantasy', 'Tolkien', 3, 2),
(6, 'Le seigneur des anneaux les deux tours', 'https://images.noosfere.org/couv/p/pocket05453-2013.jpg', 'Fantasy', 'Tolkien', 3, 2),
(7, 'Le seigneur des anneaux le retour du roi', 'http://imados.fr/history/25/le-seigneur-des-anneaux-tome-3-le-retour-du.jpg', 'Fantasy', 'Tolkien', 3, 3),
(8, 'Le Fléau', 'http://club-stephenking.fr/img/NEWS/novembre/Fleau-1-BD-Delcourt.jpg', 'Fantastique', 'Stephen King', 3, 2),
(12, 'Harry Potter et le Prince de Sang Mêlé', 'https://images.noosfere.org/couv/g/gallimard057267-2005.jpg', 'Fantastique', 'J.K Rowling', 2, 0),
(14, 'Le Silmarillion', 'https://www.pochesf.com/couvertures/826.jpg', 'Fantasy', 'Tolkien', 0, 0),
(15, 'Le Hobbit', 'http://vignette1.wikia.nocookie.net/seigneur-des-anneaux/images/d/d2/Bilbo_le_Hobbit.jpg/revision/latest?cb=20140607204848&path-prefix=fr', 'Fantasy', 'Tolkien', 2, 0),
(16, 'Eragon L\'Héritage', 'https://images-na.ssl-images-amazon.com/images/I/51Q1pMwddwL._SX344_BO1,204,203,200_.jpg', 'Fantasy', 'Christopher Paolini', 3, 2),
(17, 'Eragon L\'Aîné', 'https://images-na.ssl-images-amazon.com/images/I/51euGk%2BpOUL._SX330_BO1,204,203,200_.jpg', 'Fantasy', 'Christopher Paolini', 0, 0),
(18, 'Eragon Brisingr', 'http://static.fnac-static.com/multimedia/images_produits/ZoomPE/4/6/5/9782747014564/tsp20130903082108/Brisingr.jpg', 'Fantasy', 'Christopher Paolini', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `book_entity`
--

CREATE TABLE `book_entity` (
  `id` int(11) NOT NULL,
  `available` tinyint(4) NOT NULL,
  `nb_times_borrowed` int(11) NOT NULL,
  `item_status_rate` int(11) NOT NULL,
  `id_book` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `book_entity`
--

INSERT INTO `book_entity` (`id`, `available`, `nb_times_borrowed`, `item_status_rate`, `id_book`) VALUES
(1478, 0, 5, 5, 3),
(1587, 1, 2, 4, 4),
(7412, 0, 2, 3, 1),
(12345, 1, 1, 8, 1),
(15889, 1, 0, 8, 4),
(145263, 0, 5, 7, 12),
(145698, 1, 1, 10, 1),
(145700, 0, 5, 5, 3),
(145701, 1, 0, 2, 3),
(147325, 0, 6, 8, 3),
(412365, 1, 0, 5, 5),
(456987, 0, 13, 5, 12),
(856321, 1, 0, 7, 7),
(1475632, 1, 2, 8, 1),
(1888856, 1, 0, 6, 7),
(4569874, 1, 0, 4, 7),
(4785412, 1, 0, 3, 6),
(7415693, 0, 0, 7, 15),
(8456312, 1, 0, 6, 8),
(12587563, 0, 0, 6, 6),
(41236547, 0, 0, 6, 16),
(41258963, 0, 0, 5, 8),
(41785412, 0, 0, 9, 5),
(74102364, 0, 0, 3, 15),
(85236974, 1, 0, 3, 5),
(98764125, 1, 0, 7, 6),
(145231456, 1, 0, 5, 16),
(147852369, 1, 0, 8, 16),
(2147483647, 1, 0, 9, 8);

--
-- Déclencheurs `book_entity`
--
DELIMITER $$
CREATE TRIGGER `after_delete_book_entity` AFTER DELETE ON `book_entity` FOR EACH ROW BEGIN

		UPDATE book SET book.stock_total=book.stock_total - 1 WHERE book.id = OLD.id_book ;
		UPDATE book SET book.stock_dispo=book.stock_dispo - 1 WHERE book.id = OLD.id_book ;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_insert_book_entity` AFTER INSERT ON `book_entity` FOR EACH ROW BEGIN

UPDATE book SET
book.stock_total = book.stock_total + 1,
book.stock_dispo = book.stock_dispo + 1
WHERE book.id = NEW.id_book;

IF NEW.available='0' THEN
UPDATE book SET book.stock_dispo = book.stock_dispo -1 WHERE book.id = NEW.id_book;
END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_update_book_entity` AFTER UPDATE ON `book_entity` FOR EACH ROW BEGIN

IF NEW.available != OLD.available THEN
	IF NEW.available=0 THEN
		UPDATE book SET book.stock_dispo=book.stock_dispo - 1 WHERE book.id = NEW.id_book ;
	ELSE
		UPDATE book SET book.stock_dispo=book.stock_dispo + 1 WHERE book.id = NEW.id_book ;
	END IF;
END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `borrowed`
--

CREATE TABLE `borrowed` (
  `id_loan` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_book_entity` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `finished` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `borrowed`
--

INSERT INTO `borrowed` (`id_loan`, `id_member`, `id_book_entity`, `start_date`, `end_date`, `finished`) VALUES
(1, 1, 7412, '2017-03-01', '2017-03-08', 0),
(3, 3, 12345, '2017-03-16', '2017-03-31', 1),
(4, 3, 12345, '2017-03-09', '2017-03-16', 1),
(5, 8, 12345, '2017-03-09', '2017-03-30', 0),
(7, 2, 1587, '2017-03-15', '2017-03-18', 0),
(8, 9, 12345, '2017-03-14', '2017-04-02', 0),
(9, 2, 12345, '2017-03-08', '2017-03-12', 0),
(10, 27, 1587, '2017-03-14', '2017-03-18', 0),
(13, 6, 1587, '2017-03-14', '2017-03-17', 1),
(14, 1, 456987, '2017-03-06', '2017-03-12', 0),
(15, 9, 41258963, '2017-03-14', '2017-03-23', 0),
(16, 7, 12587563, '2017-03-14', '2017-03-18', 0),
(17, 9, 41785412, '2017-03-07', '2017-03-10', 0),
(18, 27, 145700, '2017-03-02', '2017-03-10', 0),
(19, 7, 41236547, '2017-03-14', '2017-03-23', 0),
(20, 7, 74102364, '2017-03-01', '2017-03-18', 0),
(21, 8, 7415693, '2017-03-01', '2017-03-15', 0);

--
-- Déclencheurs `borrowed`
--
DELIMITER $$
CREATE TRIGGER `after_delete_borrowed` AFTER DELETE ON `borrowed` FOR EACH ROW BEGIN

UPDATE book_entity SET book_entity.available = 1 WHERE book_entity.id= OLD.id_book_entity;
UPDATE member SET member.nb_loans = member.nb_loans - 1 WHERE member.id = OLD.id_member;
UPDATE book_entity SET book_entity.nb_times_borrowed = book_entity.nb_times_borrowed - 1 WHERE book_entity.id = OLD.id_book_entity;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_insert_borrowed` AFTER INSERT ON `borrowed` FOR EACH ROW BEGIN

UPDATE member SET member.nb_loans=member.nb_loans+1 WHERE member.id = NEW.id_member;
UPDATE book_entity SET book_entity.nb_times_borrowed = book_entity.nb_times_borrowed + 1 WHERE book_entity.id = NEW.id_book_entity;

IF NEW.finished=0 THEN
UPDATE book_entity SET book_entity.available = 0 WHERE book_entity.id= NEW.id_book_entity;
ELSE
UPDATE book_entity SET book_entity.available = 1 WHERE book_entity.id= NEW.id_book_entity;
END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_update_borrowed` AFTER UPDATE ON `borrowed` FOR EACH ROW BEGIN

IF NEW.finished=1 AND NEW.finished != OLD.finished  THEN

UPDATE book_entity SET book_entity.available = 1 WHERE book_entity.id= NEW.id_book_entity;

END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `member_since` date NOT NULL,
  `nb_loans` int(11) NOT NULL,
  `nb_penality` int(11) NOT NULL,
  `locked` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `member`
--

INSERT INTO `member` (`id`, `pseudo`, `name`, `avatar`, `member_since`, `nb_loans`, `nb_penality`, `locked`) VALUES
(1, 'LostArchives', 'Mullet Valentin', 'https://avatars0.githubusercontent.com/u/22364470?v=3&s=460', '2017-03-15', 2, 0, 0),
(2, 'Gtdestruction', 'Deli Antoine', 'https://s-media-cache-ak0.pinimg.com/originals/28/95/9e/28959e001e2c526102d58b15079dcd93.jpg', '2017-03-02', 2, 1, 0),
(3, 'Alienator00', 'Pourquoi George', 'http://vignette2.wikia.nocookie.net/finalfantasy/images/3/39/ViviMirror-FFIX.PNG/revision/latest?cb=20130628213310', '2017-03-03', 2, 0, 0),
(6, 'Orion', 'Martin Alexandre', 'http://t10.deviantart.net/q0hYkW-Gmss_zS76CbgebKB8QWQ=/300x200/filters:fixed_height(100,100):origin()/pre02/80e4/th/pre/f/2014/194/6/3/megaman_x__zero_skype_avatar_by_mikedarko-d7qdukw.png', '2017-02-01', 1, 0, 0),
(7, 'Toto', 'Dupond Mathias', 'http://vignette3.wikia.nocookie.net/airmech/images/7/71/Metroid_Avatar.jpg/revision/latest?cb=20131127081258', '2017-02-18', 3, 4, 0),
(8, 'Soldier', 'Petit Alexandre', 'http://icons.iconarchive.com/icons/yellowicon/game-stars/256/Bomberman-icon.png', '2017-03-14', 2, 2, 0),
(9, 'Irobot', 'Leroy Marc', 'http://vignette4.wikia.nocookie.net/just-dance-videogame-series/images/e/e7/Mario_avatar.png/revision/latest?cb=20140606045605', '2017-01-17', 3, 0, 0),
(27, 'Cyborg', 'Smith John', 'https://s-media-cache-ak0.pinimg.com/originals/a3/ec/47/a3ec47e74c901f7820c0633290d401c6.png', '2017-03-29', 2, 0, 0),
(28, 'Yoshi', 'Dino Dylan', 'http://vignette4.wikia.nocookie.net/steven-universe/images/9/9c/Yoshi_avatar.jpg/revision/latest?cb=20160203213032', '2017-03-30', 0, 0, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `book_entity`
--
ALTER TABLE `book_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ID_BOOK` (`id_book`);

--
-- Index pour la table `borrowed`
--
ALTER TABLE `borrowed`
  ADD PRIMARY KEY (`id_loan`),
  ADD KEY `fk_member_book` (`id_member`,`id_book_entity`),
  ADD KEY `id_book_entity` (`id_book_entity`);

--
-- Index pour la table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `book_entity`
--
ALTER TABLE `book_entity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;
--
-- AUTO_INCREMENT pour la table `borrowed`
--
ALTER TABLE `borrowed`
  MODIFY `id_loan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `book_entity`
--
ALTER TABLE `book_entity`
  ADD CONSTRAINT `FK_ID_BOOK` FOREIGN KEY (`id_book`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `borrowed`
--
ALTER TABLE `borrowed`
  ADD CONSTRAINT `borrowed_ibfk_1` FOREIGN KEY (`id_book_entity`) REFERENCES `book_entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `borrowed_ibfk_2` FOREIGN KEY (`id_member`) REFERENCES `member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
