<?php

/**
Php class to represent all informations of a book (extends of base_book class)
**/

class book_full extends base_book {

	private $nb_entities_total;
	private $nb_entities_available;
	private $book_entities;
	
	public function __construct($id,$name,$image_url,$category,$author,$nb_entities_total,$nb_entities_available,$book_entities) {
		parent::__construct($id,$name,$image_url,$category,$author);
		$this->nb_entities_total = $nb_entities_total;
		$this->nb_entities_available = $nb_entities_available;
		$this->book_entities = $book_entities;
	}
	
	public function getNb_entities_total(){
		return $this->nb_entities_total;
	}

	public function setNb_entities_total($nb_entities_total){
		$this->nb_entities_total = $nb_entities_total;
	}

	public function getNb_entities_available(){
		return $this->nb_entities_available;
	}

	public function setNb_entities_available($nb_entities_available){
		$this->nb_entities_available = $nb_entities_available;
	}

    /**
     * @return book_entity[]
     */
	public function getBook_entities(){
		return $this->book_entities;
	}

    /**
     * @param $book_entities[]
     */
	public function setBook_entities($book_entities){
		$this->book_entities = $book_entities;
	}

}


?>
