<?php

/**
Php class to represent a book_entity
**/

class book_entity {

	private $id;
	private $nb_borrowed; // Number of times the book entity has  been borrowed
	private $item_status_rate;
	private $is_available;
	
	public function __construct($id,$nb_borrowed,$item_status_rate,$available) {
		$this->id = $id;
		$this->nb_borrowed = $nb_borrowed;
		$this->item_status_rate = $item_status_rate;
		$this->is_available = $available;
	}
	
    public function getId(){
		return $this->id;
	}

    public function setId($id){
		$this->id = $id;
	}

    public function getNbBorrowed(){
        return $this->nb_borrowed;
    }


    public function setNbBorrowed($nb_borrowed){
        $this->nb_borrowed = $nb_borrowed;
    }


    public function getItemStatusRate(){
        return $this->item_status_rate;
    }

    public function setItemStatusRate($item_status_rate){
        $this->item_status_rate = $item_status_rate;
    }

    /*
    Return the specific icon for the status rate (bad,average,good)
    */
    public function getItemStatusIcon($size) {
        $icon = '';
        switch(true) {
            case ($this->item_status_rate>=7&&$this->item_status_rate<=10):
                $icon = '<i class="fa fa-smile-o fa-'.$size.'" aria-hidden="true" style="color:green"></i>';
                break;
            case ($this->item_status_rate>=4&&$this->item_status_rate<7):
                $icon = '<i class="fa fa-meh-o fa-'.$size.'" aria-hidden="true" style="color:grey"></i>';
                break;
            case ($this->item_status_rate>=0&&$this->item_status_rate<4):
                $icon = '<i class="fa fa-frown-o fa-'.$size.'" aria-hidden="true" style="color:red"></i>';
                break;
        }
        return $icon;
    }

     /*
     Translate the availability in french
     */
     public function getIsAvailable(){
        if ($this->is_available=='1')
            return 'Oui';
        else
            return 'Non';

	}

	public function setAvailable($is_available){
		$this->is_available = $is_available;
	}


}


?>
