<?php

/**
Php class for the basic informations of a book with standard getters and setters
**/

class base_book {

	private $id;
	private $name;
    	private $image_url;
	private $category;
	private $author;

	
	public function __construct($id,$name,$image_url,$category,$author) {
		$this->id = $id;
		$this->name = $name;
		$this->image_url = $image_url;
		$this->category = $category;
		$this->author = $author;
	}
	
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}


    public function getImageUrl(){
        return $this->image_url;
    }

    public function setImageUrl($image_url){
        $this->image_url = $image_url;
    }


    public function getCategory(){
		return $this->category;
	}

	public function setCategory($category){
		$this->category = $category;
	}

    public function getAuthor()
    {
        return $this->author;
    }


    public function setAuthor($author)
    {
        $this->author = $author;
    }




}


?>
