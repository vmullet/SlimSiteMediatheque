<?php

/*
Php class to represent a member
*/

class member {

	private $id;
	private $pseudo;
	private $name;
	private $avatar;
	private $signup_date; // Date when the member signed up
	private $nb_loans; // Number of loans
	private $nb_penality; // Number of penalities for the member
	private $locked;
	
	public function __construct($id,$pseudo,$name,$avatar,$signup_date,$nb_loans,$nb_penality,$locked) {
		$this->id = $id;
		$this->pseudo = $pseudo;
		$this->name = $name;
		$this->avatar = $avatar;
		$this->signup_date = $signup_date;
		$this->nb_loans = $nb_loans;
		$this->nb_penality = $nb_penality;
		$this->locked = $locked;
		
	}
	
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getPseudo(){
		return $this->pseudo;
	}

	public function setPseudo($pseudo){
		$this->pseudo = $pseudo;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getAvatar(){
		return $this->avatar;
	}

	public function setAvatar($avatar){
		$this->avatar = $avatar;
	}

	public function getSignup_date(){
		return $this->signup_date;
	}

	public function setSignup_date($signup_date){
		$this->signup_date = $signup_date;
	}

	public function getNb_loans(){
		return $this->nb_loans;
	}

	public function setNb_loans($nb_loans){
		$this->nb_loans = $nb_loans;
	}

	public function getNb_penality(){
		return $this->nb_penality;
	}

	public function setNb_penality($nb_penality){
		$this->nb_penality = $nb_penality;
	}

	/*
	
	*/
	public function getPenalityIcon($size) {
	    $icon = '';

	    switch(true) {
            case ($this->nb_penality==0):
                $icon = '<i class="fa fa-smile-o fa-'.$size.'" aria-hidden="true" style="color:green"></i>';
                break;
            case ($this->nb_penality>0&&$this->nb_penality<=3):
                $icon = '<i class="fa fa-meh-o fa-'.$size.'" aria-hidden="true" style="color:grey"></i>';
                break;
            case ($this->nb_penality>3):
                $icon = '<i class="fa fa-frown-o fa-'.$size.'" aria-hidden="true" style="color:red"></i>';
                break;
        }
        return $icon;
    }


	/*
	Translate the locked status in french
	*/
	public function getLocked(){
	    if ($this->locked=='0')
	        return 'Non';
	    else
	        return 'Oui';
	}

	public function setLocked($locked){
		$this->locked = $locked;
	}

	/*
	Static function to generate an empty member (useful in some pages)
	*/
	public static function getEmptyMember() {
	    return new member(
            '-1',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
    }
	

}



?>
