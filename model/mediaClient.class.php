<?php

/**
Singleton class to handle interactions between the site, classes and the API (GET methods) (Guzzle as an intermediate)
**/

require_once dirname(__FILE__, 2) . '/vendor/autoload.php';
require_once('member.class.php');
require_once('base_book.class.php');
require_once('book_full.class.php');
require_once('book_entity.class.php');
require_once('loan.class.php');

$client_guzzle = new GuzzleHttp\Client(); // Global variable (no duplicated objects)

class mediaClient
{

    private static $instance = null;

    const URL_SERVICE = 'http://localhost/slimservice/public/api/v1/';

    private function __construct()
    {
    }

    public static function Instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new mediaClient();
        }

        return self::$instance;
    }

    /**
     * @return member[]
     */
    public function getMembers()
    {
        global $client_guzzle;
        $response = $client_guzzle->get(self::URL_SERVICE . 'members');
        $raw_members_list = json_decode($response->getBody()->getContents(), true);
        $members = array();
        foreach ($raw_members_list as $raw_member) {
            $member = new member(
                $raw_member['id'],
                $raw_member['pseudo'],
                $raw_member['name'],
                $raw_member['avatar'],
                $raw_member['member_since'],
                $raw_member['nb_loans'],
                $raw_member['nb_penality'],
                $raw_member['locked']
            );
            $members[] = $member;
        }
        return $members; // Return an array of members

    }

    /*
    Function which return a member with a specific id (Php7 support type return except array of objects)
    */
    public function getMember($id_member): member
    {
        global $client_guzzle;
        $response = $client_guzzle->get(self::URL_SERVICE . 'members/' . $id_member);
        $raw_member = json_decode($response->getBody()->getContents(), true);
        if ($raw_member[0]['id'] == $id_member) {
            $member = new member(
                $raw_member[0]['id'],
                $raw_member[0]['pseudo'],
                $raw_member[0]['name'],
                $raw_member[0]['avatar'],
                $raw_member[0]['member_since'],
                $raw_member[0]['nb_loans'],
                $raw_member[0]['nb_penality'],
                $raw_member[0]['locked']
            );
        } else {
            $member = member::getEmptyMember();
        }
        return $member;
    }

    /**
     * @param $id_member
     * @return loan[]
     */
    public function getLoansList($id_member)
    {
        global $client_guzzle;
        $response = $client_guzzle->get(self::URL_SERVICE . 'members/' . $id_member . '/books');
        $raw_loans_list = json_decode($response->getBody()->getContents(), true);
        $loans_list = array();
        foreach ($raw_loans_list as $loan_data) {
            $book = new book_full(
                $loan_data['book']['id'],
                utf8_decode($loan_data['book']['name']),
                $loan_data['book']['image_url'],
                utf8_decode($loan_data['book']['category']),
                utf8_decode($loan_data['book']['author']),
                $loan_data['book']['stock_total'],
                $loan_data['book']['stock_dispo'],
                null // Book_entity not needed in this case
            );
            $loan = new loan(
                $loan_data['id_loan'],
                $book,
                $loan_data['id_book_entity'],
                null, // Member not needed in this case
                $loan_data['start_date'],
                $loan_data['end_date'],
                $loan_data['finished']
            );

            $loans_list[] = $loan;
        }

        return $loans_list;
    }

    /**
     * @return book_full[]
     */
    public function getBooks()
    {
        global $client_guzzle;
        $response = $client_guzzle->get(self::URL_SERVICE . 'books');
        $raw_books_list = json_decode($response->getBody()->getContents(), true);
        $books_list = array();
        foreach ($raw_books_list as $raw_book) {
            $book = new book_full(
                $raw_book['id'],
                utf8_decode($raw_book['name']),
                $raw_book['image_url'],
                utf8_decode($raw_book['category']),
                utf8_decode($raw_book['author']),
                $raw_book['stock_total'],
                $raw_book['stock_dispo'],
                null
            );
            $books_list[] = $book;
        }
        return $books_list;
    }

    /**
     * @param $id_book
     * @return book_full
     */
    public function getBook($id_book)
    {
        global $client_guzzle;
        $response = $client_guzzle->get(self::URL_SERVICE . 'books/' . $id_book);
        $raw_book = json_decode($response->getBody()->getContents(), true);
        $book_entities = array();
        $raw_book_entities = $raw_book['book_entities'];
        foreach ($raw_book_entities as $raw_entity) {
            $entity = new book_entity(
                $raw_entity['id'],
                $raw_entity['nb_times_borrowed'],
                $raw_entity['item_status_rate'],
                $raw_entity['available']
            );
            $book_entities[] = $entity; // Add the current book_entity objec to book_entities array
        }

        $book = new book_full(
            $raw_book['id'],
            utf8_decode($raw_book['name']),
            $raw_book['image_url'],
            utf8_decode($raw_book['category']),
            utf8_decode($raw_book['author']),
            $raw_book['stock_total'],
            $raw_book['stock_dispo'],
            $book_entities
        );


        return $book; // Return the book boject with its entities

    }

    /**
     * @param $keyword
     * @return book_full[]
     */
    public function searchBook($keyword)
    {
        global $client_guzzle;
        $response = $client_guzzle->get(self::URL_SERVICE . 'books/search/' . urlencode($keyword));
        $raw_book_list = json_decode($response->getBody()->getContents(), true);
        $result_list = array();
        foreach ($raw_book_list as $raw_book) {
            $book = new book_full(
                $raw_book['id'],
                utf8_decode($raw_book['name']),
                $raw_book['image_url'],
                utf8_decode($raw_book['category']),
                utf8_decode($raw_book['author']),
                $raw_book['stock_total'],
                $raw_book['stock_dispo'],
                null
            );

            $result_list[] = $book; // Add the current book object to the result_list
        }
        return $result_list; // Return the result list of the search (array of book_full objects)
    }
}


?>
