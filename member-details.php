<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once('model/mediaClient.class.php');

$id_member = -1;
$member = null;
$member_loans = null;

if (isset($_GET['id_member'])) {
    $id_member = $_GET['id_member'];
    $media_client = mediaClient::Instance();
    $member = $media_client->getMember($id_member);
    if ($member->getId() == $id_member) {
        $member_loans = $media_client->getLoansList($id_member);
    } else {
        header("Location: 404.php");
        exit();
    }

} else {
    header("Location: 404.php");
    exit();
}


?>

<?php

$pageTitle = "Détails du Membre";

include_once('includes/header.php');



?>

<!--
<div id="message">
    <div style="padding: 5px;">
        <div class="alert alert-success alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Indicates a successful or positive action.
</div>
    </div>
</div>
-->
<input type="hidden" id="hidden_id_member" value="<?php echo $id_member; ?>">
<section>
    <div class="container">
        <div class="about-main">
            <h3 style="color:yellow">Profil du membre</h3>
            <div class="about-top">
                <div class="col-md-6">
                    <img src="<?php echo $member->getAvatar(); ?>" style="border-radius:50%;width:200px;float:right"/>
                </div>
                <div class="col-md-6 about-top-right">
                    <h4 style="color:white;">Pseudo
                        : <?php echo $member->getPseudo() . ' ( ' . $member->getName() . ' ) '; ?></h4>
                    <p style="color:white;">Membre depuis : <?php echo $member->getSignup_date(); ?></p>
                    <p style="color:white;">Nombre d'emprunts : <?php echo $member->getNb_loans(); ?></p>
                    <p style="color:white;">Nombre de pénalités
                        : <?php echo $member->getNb_penality(); ?>     <?php echo $member->getPenalityIcon('lg'); ?></p>
                    <p style="color:white;">Compte bloqué : <?php echo $member->getLocked(); ?></p>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </div>
    <div style="margin:50px;"></div>
    <h1>Liste des Emprunts</h1>
	<div class="media-table">
    <div class="tbl-header">
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
                <th>N° Réservation</th>
                <th>Livre</th>
                <th>Titre</th>
                <th>Catégorie</th>
                <th>N° Exemplaire</th>
                <th>Du</th>
                <th>Jusqu'au</th>
                <th>Rendu ?</th>
                <th>Actions</th>
            </tr>
            </thead>
        </table>
    </div>
	
    <?php if (count($member_loans) > 0) { ?>
        <div class="tbl-content">
            <table cellpadding="0" cellspacing="0" border="0">
                <tbody>
                <?php foreach ($member_loans as $loan) {
                    $book = $loan->getBook();
                    ?>
                    <tr id="row_loan-<?php echo $loan->getId(); ?>">
                        <td>00<?php echo $loan->getId(); ?></td>
                        <td><img src="<?php echo $book->getImageUrl(); ?>" style="width:65px"/></td>
                        <td>
                            <a href="book-details.php?id_book=<?php echo $book->getId(); ?>"><?php echo $book->getName(); ?></a>
                        </td>
                        <td><?php echo $book->getCategory(); ?></td>
                        <td id="loan_entity_id-<?php echo $loan->getId(); ?>"><?php echo $loan->getBook_entity(); ?></td>
                        <td id="loan_start_date-<?php echo $loan->getId(); ?>"><?php echo $loan->getStart_date(); ?></td>
                        <td id="loan_end_date-<?php echo $loan->getId(); ?>"><?php echo $loan->getEnd_date(); ?></td>
                        <td id="loan_is_finished-<?php echo $loan->getId(); ?>"><?php echo $loan->getIsFinished(); ?></td>
                        <td>
                            <button type="button" class="btn btn-warning edit_loan" id="edit_loan-<?php echo $loan->getId(); ?>" data-toggle="tooltip"
                                    title="Editer l'emprunt"><i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-danger delete_loan" id="delete_loan-<?php echo $loan->getId(); ?>" data-toggle="tooltip"
                                    title="Supprimer l'emprunt"><i class="fa fa-times" aria-hidden="true"></i></button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
		</div>
    <?php } else { ?>
        <div class="main2">
            <div class="error-404 text-center">
                <h1>Aucun emprunt
                    <i class="fa fa-meh-o" aria-hidden="true"></i>
                </h1>
            </div>
        </div>
    <?php } ?>
</section>
<!-- banner -->
<!-- Here -->
<div class="Here">
    <div class="workes">
        <h2><a href="portfolio.html">See All Our Works</a></h2>
    </div>
    <div class="container">
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="bulb"></i>
            </div>
            <div class="here-right">
                <h5>Your 1st title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere consectetur,
                dolor ipsum tempus justo, quis ornare magna urna id enim. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="lock"></i>
            </div>
            <div class="here-right">
                <h5>Your 2nd title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nulla accumsan libero id quam facilisis nec luctus libero sollicitudin. Sed ligula libero, feugiat sed
                gravida vel, commodo sit amet nibh. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="pen"></i>
            </div>
            <div class="here-right">
                <h5>Your 3rd title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nullam tempus turpis at lorem posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit
                fermentum augue. Etiam eget magna vel ante mattis ultricies vitae ut nunc. </p>
        </div>
        <div class="col-md-3 here-1">
            <div class="here-left">
                <i class="adm"></i>
            </div>
            <div class="here-right">
                <h5>Your 4th title Here</h5>
            </div>
            <div class="clearfix"></div>
            <p>Nam ac molestie ante. Pellentesque turpis lacus, vulputate vitae feugiat quis, dictum eget diam. Proin
                sapien libero, tempus et tempus ac, pulvinar quis nulla. </p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Here -->
<!-- company -->
<div class="company">
    <div class="container">
        <div class="col-md-4 company-1">
            <h3>Our Company</h3>
            <div class="our">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere
                    consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim. Nulla accumsan libero id quam
                    facilisis nec luctus libero sollicitudin. </p>
            </div>
            <div class="our">
                <p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem
                    posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit fermentum augue.</p>
            </div>
            <div class="our">
                <p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem
                    rutrum venenatis sed laoreet dui. </p>
            </div>
        </div>
        <div class="col-md-4 company-1">
            <h3>Our Testimonials</h3>
            <div class="our">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere
                    consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim.</p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
            <div class="our">
                <p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem
                    posuere sodales. Donec nibh urna, auctor at </p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
            <div class="our">
                <p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem
                    rutrum venenatis sed laoreet dui. </p>
                <h6>Rafi, GraphicsFuel.com </h6>
            </div>
        </div>
        <div class="col-md-4 company-1">
            <h3>From the blog</h3>
            <div class="sleek">
                <h5><a href="single.html">Sleek minimal website PSD template</a></h5>
                <h6>August 30, 2010, 9:32 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">PSD ecommerce website template</a></h5>
                <h6>October 18, 2010, 5:51 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Sleeko- Download single page website design</a></h5>
                <h6>July 22, 2010, 7:19 pm</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Download web buttons in PSD & PNG (pack of 60)</a></h5>
                <h6>September 13, 2010, 2:15 am</h6>
            </div>
            <div class="sleek">
                <h5><a href="single.html">Flip clock countdown (PSD)</a></h5>
                <h6>August 12, 2011, 10:56 am</h6>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- company -->
<!-- later -->

<!-- later -->

<?php
include_once('includes/footer.php');
?>