[SlimSite][slim_site_link] 
========
# A website demo for [SlimService API][api_link]
<div style="display:inline-block">
<img src="https://www.w3.org/html/logo/downloads/HTML5_Logo_256.png" width="75" height="75" />
<img src="http://seeklogo.com/images/C/css3-logo-8724075274-seeklogo.com.gif" width="75" height="75" />
<img src="https://jeremyrajan.gallerycdn.vsassets.io/extensions/jeremyrajan/vscode-lebab/1.0.1/1485547425692/Microsoft.VisualStudio.Services.Icons.Default" width="75" height="75" />
<img src="https://appdevelopermagazine.com/images/news_images/PPH7-App-Developer-Magazine_qnftmgpw.jpg" width="75" height="75" />
<img src="https://genesisui.com/img/logos/bootstrap.png" width="75" height="75" />
<img src="http://xpertinfotech.in/assets/images/art/jquery-training-overview2-xpert-%20infotech.png" width="75" height="75" />
<img src="http://www.optiinfo.com/images/services/ajax-website-development.png" width="75" height="75" />
</div>
Slimsite has been created to show you the potentital behind the SlimService API.

# Edit the API URL

To change the API URL, please edit the constant URL_SERVICE in mediaClient.class.php in 'model' folder

# Technologies

Actually, the website uses different technologies/ components such as :

## Languages :

- **HTML5 , CSS3**
- **PHP 7.1** (be sure to have an APACHE server compatible with PHP7). -> If you test with xampp, download the PHP7 version
- **[Javascript ES6][es6_link] (to have a cleaner code and better OOP syntax/support)**

## Libraries :

- **[Bootstrap][bootstrap_link] v3.3.7**
- **[jQuery][jquery_link] v3.2.0**
- **[BootBox][bootbox_link] v4.4.0 (to display modals)**
- **[Masonry][masonry_link] to display the book gallery**
- **[Bootstrap Date Time Picker][bdt_picker_link]**

## Dependencies :

- **[Guzzle][guzzle_link] (to handle direct interaction with the API)**

# Project Structure :

The project is made of a lot of files but the most important ones are in css,js,model and also at the root of the project

## At the root folder

At the root folder, you can find all pages of the websites such as :

- **index.php** : The main page of the website where you can find the catalog of all books , filter them , add , edit , delete books

- **book-details.php** : The page displayed when you click on a book in **index.php**. On this page are displayed book informations and its entities. They can be edited , deleted and a loan can be created only on available entities.

- **members.php** : The page where are displayed all members. You can also add new members, edit or delete existing ones.

- **member-details.php** : The page displayed when you click on a member in **members.php**. On this page are displayed the member informations and its loans. They can be edited or deleted.

- **search.php** : The page where you can search for a book by typing a keyword. The search is done on the title and the author. **(Check bookManager.class.php in the slimService API for the SQL Query)**. When the search is done, a gallery similar to the one in index.php is displayed

## In 'includes' folder

The includes folder contains :

- **header.php** : which contains my css links, menu bar
- **footer.php** : which contains the footer associted to all pages of the website

## In 'css' folder

In the css folder, you can find the different files for styles. Some comes from libraries such as the ones starting by bootstrap-xxx. The one containing my custom style for the website is called :
- **style.css**

## In 'action' folder

In the action folder, you can find all pages which handle POST/PUT/DELETE queries sent to the API. These files are the following :

- **book-action.php** : The file which handles add/edit/delete requests for books
- **member-action.php** : The file which handles add/edit/delete for members
- **entity-action.php** : The file which handles add/edit/delete for book_entities
- **loan-action.php** : The file which handles add/edit/delete for loans


## In 'js' folder

The Js folder is more "complex" as it contains my classes **written en ES6**. All classes are called **classNameJs.class.js** in order to not misunderstand with php classes
My classes are the following :

- **bookJs.class.js** : The ES6 singleton class to handle add , edit , delete processes for **books** in AJAX and to generate modal forms
- **entityJs.class.js** : The ES6 singleton class to handle add , edit , delete processes for **book_entities** in AJAX and to generate modal forms
- **memberJs.class.js** : The ES6 singleton class to handle add , edit , delete processes for **members** in AJAX and to generate modal forms
- **loanJs.class.js** : The ES6 singleton class to handle add , edit , delete processes for **loans** in AJAX and to generate modal forms

These classes are used by the file **script.js** which handles trigger for click actions  on buttons, open,close modals and calls my classes methods.

## In 'model' folder

The model folder contains my PHP classes adapted to my database tables. This makes the display process much easier to do and to modify.
The model folder contains :

- **base_book.class.php** : The class which contains basic information for book (useful when you need only the main informations on a page such as **index.php**)
- **book_full.class.php** : The class extended from base_book which contains additional information such as stock_total,stock_available etc for a book (**see book-details.php**)
- **member.class.php** : The class which contains informations for a member (**used in members.php and member-details.php**)
- **book_entity.class.php** : The class which contains informations for a book entity (**see book-details.php**)
- **loan.class.php** : The class which contains informations for a loan (**see member-details.php**)
- **mediaClient.class.php** : The singleton class to handle all GET requests on the API by using Guzzle. 
### mediaClient also contains the constant for the API URL



   [api_link]: <https://github.com/LostArchives/SlimServiceMediatheque>
   [es6_link]: <http://es6-features.org/>
   [bootstrap_link]: <http://getbootstrap.com>
   [jquery_link]: <http://jquery.com>
   [bootbox_link]: <http://bootboxjs.com/>
   [masonry_link]: <http://masonry.desandro.com/>
   [bdt_picker_link]: <https://eonasdan.github.io/bootstrap-datetimepicker>
   [guzzle_link]: <http://docs.guzzlephp.org/en/latest/>
   [slim_site_link]: <http://vps.lostarchives.fr/slimsite>
